# chessplained

## Things to do

### Improve overall results of CNN
* Gridsearch and hyperparametrization
* More restrict data subset - e.g. selecting only disjunctive tacticalmotifs

### Data augmentation
* Reversing board colors
* Some transformations using the board main diagonals

### Results visualization - class activation mapping
* Heatmaps, heatmaps, heatmaps
  * https://github.com/jacobgil/pytorch-grad-cam
  * http://www.heatmapping.org/

### Microservice serving position themes classifications
* Probably some small FastAPI project or some TF Serving
* Would love to have some actual frontend for that
* How about some lichess plugin tracking current position?


## ETC
* There's a sample of 1000 positions available under `chessplained/dataset/sample.pickle` - it's a `pd.DataFrame` object pickled.
* There are some pretrained models for the set of `{TacticalMotif.Pin, TacticalMotif.Fork, TacticalMotif.Skewer}` motifs.
