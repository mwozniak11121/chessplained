from dataclasses import asdict, dataclass
from typing import Optional, Union

import chess
import chess.svg
import numpy as np
import pandas as pd

from chessplained import paths
from chessplained.enums import TacticalMotif


@dataclass
class Position:
    id: str
    rating: int
    plays: int

    themes: list[str]
    moves: str
    url: str

    solution_ply_num: int
    board: chess.Board
    representation: np.array


def load_into_model(record: pd.Series, as_dict: Optional[bool] = True) -> Union[Position, dict]:
    board = chess.Board()

    # Pushing first move from the solution to the moves stack
    # As lichess for some reason shows initial FEN one move earlier
    board.set_fen(record["fen"])
    board.push(chess.Move.from_uci(record["moves"][:4]))

    # Color flip - resulting position will always be a move for white
    if not board.turn:
        board.apply_mirror()

    position = Position(
        **{
            "id": record["id"],
            "rating": record["rating"],
            "plays": record["nb_plays"],
            "themes": record["themes"].split(" "),
            "moves": record["moves"],
            "url": record["game_url"],
            "solution_ply_num": len(record["moves"].split(" ")),
            "board": board,
            "representation": build_im_representation(board),
        }
    )

    return asdict(position) if as_dict else position


def build_im_representation(board: chess.Board) -> np.array:
    """
    Channels:
    0 - pawn
    1 - rook
    2 - knight
    3 - bishop
    4 - queen
    5 - king

    lowercase means black piece.
    """
    channel_mapping = {
        "p": 0,
        "r": 1,
        "n": 2,
        "b": 3,
        "q": 4,
        "k": 5,
    }

    im2d = np.array(list(str(board).replace("\n", "").replace(" ", ""))).reshape((8, 8))
    result = np.zeros((8, 8, 6)).astype("byte")

    for i in range(8):
        for j in range(8):
            piece = im2d[i, j]
            if piece == ".":
                continue

            if piece.isupper():
                result[i, j, channel_mapping[piece.lower()]] = 1
            else:
                result[i, j, channel_mapping[piece.lower()]] = -1

    return result


def build_alg_representation(board: chess.Board) -> np.array:
    """
    lowercase means black piece.
    """
    pieces_mapping = {"p": 1, "n": 3, "b": 3, "r": 5, "q": 7, "k": 9}

    im2d = np.array(list(str(board).replace("\n", "").replace(" ", ""))).reshape((8, 8))
    result = np.zeros((8, 8)).astype("byte")

    for i in range(8):
        for j in range(8):
            piece = im2d[i, j]
            if piece == ".":
                continue

            if piece.isupper():
                result[i, j] = pieces_mapping[piece.lower()]
            else:
                result[i, j] = -1 * pieces_mapping[piece.lower()]

    return result


def calculate_motif_occurrences(motifs: pd.Series) -> pd.Series:
    return pd.Series(sum((item for item in motifs), [])).value_counts()


def preprocessing_pipeline(
    limit: Optional[int] = 100,
) -> tuple[pd.DataFrame, pd.Series, pd.DataFrame]:
    df = pd.read_csv(
        paths.DATASET_CSV_PATH,
        nrows=limit,
        names=[
            "id",
            "fen",
            "moves",
            "rating",
            "rating_deviation",
            "popularity",
            "nb_plays",
            "themes",
            "game_url",
        ],
    )

    df = pd.DataFrame(list(df.apply(load_into_model, as_dict=True, axis=1)))

    df = df.query("solution_ply_num <= 5").reset_index(drop=True)

    allowed_motifs = set(list(TacticalMotif))
    filter_function = lambda themes: [theme for theme in themes if theme in allowed_motifs]
    df["themes"] = df["themes"].apply(filter_function)

    df = df[df["themes"].astype(bool)].reset_index(drop=True)

    x = df.representation
    y = pd.get_dummies(df.themes.apply(pd.Series).stack()).sum(level=0)

    df.drop("representation", inplace=True, axis=1)

    df.to_pickle(paths.METADATA_PATH)
    x.to_pickle(paths.X_INPUT_PATH)
    y.to_pickle(paths.Y_INPUT_PATH)

    return df, x, y
