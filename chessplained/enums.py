from enum import Enum


class TacticalMotif(str, Enum):
    """
    String values chosen to be compatible with https://lichess.org/training/themes
    """

    AdvancedPawn = "advancedPawn"
    DiscoveredAttack = "discoveredAttack"
    CaptureTheDefender = "capturingDefender"
    DoubleCheck = "doubleCheck"
    Fork = "fork"
    HangingPiece = "hangingPiece"
    Pin = "pin"
    Skewer = "skewer"
    TrappedPiece = "trappedPiece"

    Attraction = "attraction"
    Clearance = "clearance"
    Deflection = "deflection"
    Interference = "interference"
    X_Ray = "xRayAttack"
