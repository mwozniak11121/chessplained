import numpy as np
import pandas as pd
from sklearn.metrics import accuracy_score, hamming_loss

from chessplained import paths
from chessplained.enums import TacticalMotif
from chessplained.network import create_baseline_ann, create_baseline_cnn, train_model
from chessplained.preprocessing import build_alg_representation, build_im_representation

VAL_SPLIT = 0.33


def cnn():
    df: pd.DataFrame = pd.read_pickle(paths.METADATA_PATH)

    allowed_motifs = {TacticalMotif.Pin, TacticalMotif.Fork, TacticalMotif.Skewer}

    filter_function = lambda themes: [theme for theme in themes if theme in allowed_motifs]
    df["themes"] = df["themes"].apply(filter_function)
    df = df[df.themes.apply(lambda x: len(x) > 0)].reset_index(drop=True)

    x = df.board.apply(build_im_representation)
    x = np.stack(x.to_numpy())

    y = pd.get_dummies(df.themes.apply(pd.Series).stack()).sum(level=0)

    val_num = int(VAL_SPLIT * len(df))
    x_train, x_test = x[:-val_num], x[-val_num:]
    y_train, y_test = y[:-val_num], y[-val_num:]

    input_shape = x.shape[1:]
    num_of_output_classes = y.shape[1]

    model = create_baseline_cnn(input_shape, num_of_output_classes)

    if paths.CNN_WEIGHTS_PATH.is_file():
        model.load_weights(paths.CNN_WEIGHTS_PATH)
    else:
        history = train_model(
            model,
            x_train,
            y_train,
            x_test,
            y_test,
            epochs=100,
            path_to_save=paths.CNN_WEIGHTS_PATH,
        )

    prediction = pd.DataFrame(model.predict(x_test)).applymap(lambda x: x > 0.5)
    metrics = {
        "hamming_loss": hamming_loss(y_test, prediction),
        "accuracy": accuracy_score(y_test, prediction),
    }

    prediction = prediction.join(y_test.reset_index(drop=True))


def ann():
    df: pd.DataFrame = pd.read_pickle(paths.METADATA_PATH)

    allowed_motifs = {TacticalMotif.Pin, TacticalMotif.Fork, TacticalMotif.Skewer}

    filter_function = lambda themes: [theme for theme in themes if theme in allowed_motifs]
    df["themes"] = df["themes"].apply(filter_function)
    df = df[df.themes.apply(lambda x: len(x) > 0)].reset_index(drop=True)

    x = df.board.apply(build_alg_representation)
    x = np.stack(x.to_numpy())

    y = pd.get_dummies(df.themes.apply(pd.Series).stack()).sum(level=0)

    val_num = int(VAL_SPLIT * len(df))
    x_train, x_test = x[:-val_num], x[-val_num:]
    y_train, y_test = y[:-val_num], y[-val_num:]

    input_shape = x.shape[1:]
    num_of_output_classes = y.shape[1]

    model = create_baseline_ann(input_shape, num_of_output_classes)

    if paths.ANN_WEIGHTS_PATH.is_file():
        model.load_weights(paths.ANN_WEIGHTS_PATH)
    else:
        history = train_model(
            model,
            x_train,
            y_train,
            x_test,
            y_test,
            epochs=100,
            path_to_save=paths.ANN_WEIGHTS_PATH,
        )

    prediction = pd.DataFrame(model.predict(x_test))

    prediction = prediction.applymap(lambda x: x > 0.5)

    metrics = {
        "hamming_loss": hamming_loss(y_test, prediction),
        "accuracy": accuracy_score(y_test, prediction),
    }
    prediction = prediction.join(y_test.reset_index(drop=True))
