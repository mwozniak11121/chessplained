from statistics import mean

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from scipy import stats
from sklearn.metrics import accuracy_score, hamming_loss
from sklearn.model_selection import KFold

from chessplained import paths
from chessplained.enums import TacticalMotif
from chessplained.network import create_baseline_ann, create_baseline_cnn, train_model
from chessplained.preprocessing import build_alg_representation, build_im_representation

N_REPEATS = 30
N_SPLITS = 10
EPOCHS = 100


def main():
    df: pd.DataFrame = pd.read_pickle(paths.METADATA_PATH)

    allowed_motifs = {TacticalMotif.Pin, TacticalMotif.Fork, TacticalMotif.Skewer}

    filter_function = lambda themes: [theme for theme in themes if theme in allowed_motifs]
    df["themes"] = df["themes"].apply(filter_function)
    df = df[df.themes.apply(lambda x: len(x) > 0)].reset_index(drop=True)

    x_cnn = df.board.apply(build_im_representation)
    x_cnn = np.stack(x_cnn.to_numpy())

    x_ann = df.board.apply(build_alg_representation)
    x_ann = np.stack(x_ann.to_numpy())

    y = pd.get_dummies(df.themes.apply(pd.Series).stack()).sum(level=0).to_numpy()

    input_shape_im = x_cnn.shape[1:]
    input_shape_alg = x_ann.shape[1:]
    num_of_output_classes = y.shape[1]

    metrics_scores = {}
    for model_name in ["ANN", "CNN"]:
        metrics_scores[model_name] = {}
        for metric_name in ["hamming_loss", "accuracy"]:
            metrics_scores[model_name][metric_name] = {i: [] for i in range(N_REPEATS)}

    for i in range(N_REPEATS):
        kf = KFold(n_splits=N_SPLITS, random_state=i, shuffle=True)
        for train, test in kf.split(X=x_ann):
            model_cnn = create_baseline_cnn(input_shape_im, num_of_output_classes)
            model_ann = create_baseline_ann(input_shape_alg, num_of_output_classes)

            x_train_ann, x_test_ann = x_ann[train], x_ann[test]
            x_train_cnn, x_test_cnn = x_cnn[train], x_cnn[test]
            y_train, y_test = y[train], y[test]

            train_model(
                model_cnn,
                x_train_cnn,
                y_train,
                x_test_cnn,
                y_test,
                epochs=EPOCHS,
            )

            train_model(
                model_ann,
                x_train_ann,
                y_train,
                x_test_ann,
                y_test,
                epochs=EPOCHS,
            )

            m1, m2 = get_metrics(x_test_cnn, y_test, model_cnn)
            metrics_scores["CNN"]["hamming_loss"][i].append(m1)
            metrics_scores["CNN"]["accuracy"][i].append(m2)

            m1, m2 = get_metrics(x_test_ann, y_test, model_ann)
            metrics_scores["ANN"]["hamming_loss"][i].append(m1)
            metrics_scores["ANN"]["accuracy"][i].append(m2)

    for model_name in ["ANN", "CNN"]:
        for metric_name in ["hamming_loss", "accuracy"]:
            metrics_scores[model_name][metric_name] = [
                mean(val) for key, val in metrics_scores[model_name][metric_name].items()
            ]

    results = pd.DataFrame(metrics_scores)

    ax = sns.boxplot(data=[results.ANN.hamming_loss, results.CNN.hamming_loss])
    ax.set_xticklabels(["ANN", "CNN"])
    ax.set_ylabel("Utrata Hamminga")
    ax.set_xlabel("Rodzaj sieci")
    plt.show()
    plt.savefig("./hamming_loss.png")
    plt.cla()
    plt.clf()

    ax = sns.boxplot(data=[results.ANN.accuracy, results.CNN.accuracy])
    ax.set_xticklabels(["ANN", "CNN"])
    ax.set_ylabel("Dokładny współczynnik dopasowania")
    ax.set_xlabel("Rodzaj Sieci")
    plt.show()
    plt.savefig("./accuracy.png")
    plt.cla()
    plt.clf()

    statistic, pvalue = stats.shapiro([results.ANN.accuracy, results.CNN.accuracy])
    statistic, pvalue = stats.mannwhitneyu(results.ANN.accuracy, results.CNN.accuracy)


def get_metrics(x_test, y_test, model):
    prediction = pd.DataFrame(model.predict(x_test)).applymap(lambda x: x > 0.5)

    return (
        hamming_loss(y_test, prediction),
        accuracy_score(y_test, prediction),
    )
