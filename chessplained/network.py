import pathlib
from typing import Union

import keras
import numpy as np
import pandas as pd
import tensorflow_addons as tfa
from keras.callbacks import EarlyStopping, ModelCheckpoint
from keras.layers import BatchNormalization, Conv2D, Dense, Dropout, Flatten
from keras.models import Sequential

DataContainer = Union[pd.DataFrame, pd.Series, np.array]


def create_baseline_cnn(input_shape: tuple[int], num_of_output_classes: int, single_class: bool = False):
    model = Sequential(
        [
            Conv2D(
                filters=32,
                kernel_size=3,
                strides=1,
                padding="valid",
                activation=None,
                kernel_initializer="glorot_uniform",
                bias_initializer="glorot_uniform",
                kernel_regularizer=None,
                bias_regularizer=None,
                input_shape=input_shape,
            ),
            BatchNormalization(),
            tfa.layers.GELU(),
            Conv2D(
                filters=32,
                kernel_size=3,
                strides=1,
                padding="valid",
                activation=None,
                kernel_initializer="glorot_uniform",
                bias_initializer="glorot_uniform",
                kernel_regularizer=None,
                bias_regularizer=None,
            ),
            BatchNormalization(),
            tfa.layers.GELU(),
            Conv2D(
                filters=32,
                kernel_size=5,
                strides=2,
                padding="same",
                activation=None,
                kernel_initializer="glorot_uniform",
                bias_initializer="glorot_uniform",
                kernel_regularizer=None,
                bias_regularizer=None,
            ),
            BatchNormalization(),
            tfa.layers.GELU(),
            Flatten(),
            Dense(
                units=num_of_output_classes,
                activation="softmax" if single_class else "sigmoid",
                kernel_initializer="glorot_uniform",
                bias_initializer="glorot_uniform",
                kernel_regularizer=None,
                bias_regularizer=None,
            ),
        ]
    )
    model.compile(
        loss="binary_crossentropy",
        optimizer=tfa.optimizers.Yogi(learning_rate=0.001),
        metrics=["accuracy"],
    )

    return model


def create_baseline_ann(input_shape: tuple[int], num_of_output_classes: int):
    model = Sequential(
        [
            keras.layers.Flatten(),
            Dense(128, input_shape=input_shape, activation="relu"),
            Dropout(rate=0.2),
            Dense(64, activation="relu"),
            Dropout(rate=0.2),
            Dense(32, activation="relu"),
            Dropout(rate=0.2),
            Dense(
                units=num_of_output_classes,
                activation="sigmoid",
                kernel_initializer="glorot_uniform",
                bias_initializer="glorot_uniform",
                kernel_regularizer=None,
                bias_regularizer=None,
            ),
        ]
    )
    model.compile(
        loss="binary_crossentropy",
        optimizer=tfa.optimizers.Yogi(learning_rate=0.001),
        metrics=["accuracy"],
    )

    return model


def train_model(
    model: keras.Model,
    x_train: DataContainer,
    y_train: DataContainer,
    x_test: DataContainer = None,
    y_test: DataContainer = None,
    epochs: int = 100,
    path_to_save: pathlib.Path = None,
):
    callbacks = []

    if path_to_save:
        callbacks.append(
            ModelCheckpoint(
                filepath=path_to_save,
                monitor="val_loss",
                mode="min",
                save_best_only=True,
                verbose=1,
            )
        )

    callbacks.append(
        EarlyStopping(
            monitor="val_loss",
            min_delta=0,
            patience=3,
            verbose=1,
            restore_best_weights=True,
        )
    )

    return model.fit(
        x=x_train,
        y=y_train,
        batch_size=64,
        epochs=epochs,
        verbose=1,
        sample_weight=None,
        callbacks=callbacks,
        validation_data=((x_test, y_test) if (x_test is not None and y_test is not None) else None),
    )
