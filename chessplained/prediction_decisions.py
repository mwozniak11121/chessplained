import pathlib
from typing import Union

import chess
import chess.svg
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import colors as mcolors

from chessplained.enums import TacticalMotif
from chessplained.preprocessing import build_im_representation

ADDITIONAL_COLORS = {color: mcolors.CSS4_COLORS[color] for color in ["crimson", "forestgreen", "thistle", "khaki"]}
COLORS_DICT = mcolors.TABLEAU_COLORS | ADDITIONAL_COLORS
COLORS_VALUES = dict(zip(range(len(TacticalMotif)), COLORS_DICT.values()))


def generate_svg_board(model, board: chess.Board) -> chess.svg.SvgWrapper:
    def _predicted_class(arr: np.ndarray) -> int:
        return np.argmax(model.predict(np.expand_dims(arr, axis=0)))

    arr = build_im_representation(board)
    base_truth = _predicted_class(arr)

    important_squares = []
    for square in chess.SQUARES:
        temp_board = board.copy()
        piece = temp_board.remove_piece_at(square)
        if piece:
            temp_arr = build_im_representation(temp_board)
            if _predicted_class(temp_arr) != base_truth:
                important_squares.append(square)

    fill = {}
    if important_squares:
        square_set = chess.SquareSet(important_squares)
        fill = dict.fromkeys(square_set, COLORS_VALUES[base_truth])

    return chess.svg.board(board, fill=fill)


def export_board_svg(board: Union[chess.Board, chess.svg.SvgWrapper], path: pathlib.Path):
    if isinstance(board, chess.Board):
        svg_obj = chess.svg.board(board)
    elif isinstance(board, chess.svg.SvgWrapper):
        svg_obj = board
    else:
        raise ValueError("board arg needs to be chess.Board or chess.svg.SvgWrapper")

    path.parent.mkdir(parents=True, exist_ok=True)

    with open(path, "w") as f:
        f.write(svg_obj)


def generate_color_legend() -> plt.Figure:
    colors = list(COLORS_DICT)
    legend_len = len(TacticalMotif)
    labels = [str(motif).split(".")[1] for motif in TacticalMotif]

    f = lambda m, c: plt.plot([], [], marker=m, color=c, ls="none")[0]
    handles = [f("s", colors[i]) for i in range(legend_len)]

    legend = plt.legend(handles, labels, loc=3, framealpha=1, frameon=True, markerscale=3)

    expand = [-5, -5, 5, 5]

    fig = legend.figure
    fig.canvas.draw()

    bbox = legend.get_window_extent()
    bbox = bbox.from_extents(*(bbox.extents + np.array(expand)))
    bbox = bbox.transformed(fig.dpi_scale_trans.inverted())

    fig.savefig("legend.png", dpi=300, bbox_inches=bbox)
    return fig
