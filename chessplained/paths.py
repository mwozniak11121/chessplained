from pathlib import Path

FILE_PATH = Path(__file__).parent
RESULTS_PATH = FILE_PATH / "scraper_results"
DATASET_PATH = FILE_PATH / "dataset"
WEIGHTS_PATH = FILE_PATH / "weights"

DATASET_CSV_PATH = DATASET_PATH / "lichess_db_puzzle.csv"
METADATA_PATH = DATASET_PATH / "metadata.pickle"
SAMPLE_PATH = DATASET_PATH / "sample.pickle"
X_INPUT_PATH = DATASET_PATH / "x_encoded.pickle"
Y_INPUT_PATH = DATASET_PATH / "y_encoded.pickle"

CNN_WEIGHTS_PATH = WEIGHTS_PATH / "cnn_baseline.h5"
ANN_WEIGHTS_PATH = WEIGHTS_PATH / "ann_baseline.h5"
CNN_SINGLE_WEIGHTS_PATH = WEIGHTS_PATH / "cnn_single_baseline.h5"
OVR_PATH = WEIGHTS_PATH / "ovr"
BOARD_PATH = FILE_PATH / "boards"
