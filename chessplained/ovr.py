from typing import Literal

import keras
import numpy as np
import pandas as pd

from chessplained import paths
from chessplained.network import create_baseline_cnn, train_model


class OVRWrapper:
    models: dict[str, keras.Sequential]
    labels: list[str]
    histories: dict[str, dict]

    def __init__(self, labels: list[str], input_shape: tuple[int] = None):
        if not input_shape:
            input_shape = (8, 8, 6)

        self.models = {
            label: create_baseline_cnn(input_shape=input_shape, num_of_output_classes=1, single_class=False)
            for label in labels
        }
        self.labels = labels
        self.histories = {}
        paths.OVR_PATH.mkdir(exist_ok=True, parents=True)

    def fit(self, x_train: np.ndarray, y_train: pd.DataFrame, x_test: np.ndarray, y_test: pd.DataFrame):
        for label in self.labels:
            model_path = paths.OVR_PATH / f"{label}.h5"

            if model_path.is_file():
                self.models[label].load_weights(model_path)
            else:
                self.histories[label] = train_model(
                    self.models[label],
                    x_train,
                    y_train[label],
                    x_test,
                    y_test[label],
                    epochs=100,
                    path_to_save=model_path,
                )

    def _predict_regression(self, x: np.ndarray, label: str) -> pd.Series:
        return pd.Series(self.models[label].predict(x).flatten())

    @staticmethod
    def _get_top_class(row: pd.Series) -> pd.Series:
        return row == row.max()

    def predict(
        self, x: np.ndarray, strategy: Literal["top_class", "threshold", "regression"] = "top_class"
    ) -> pd.DataFrame:
        results = pd.DataFrame({label: self._predict_regression(x, label) for label in self.labels})

        if strategy == "top_class":
            results = results.apply(self._get_top_class, axis=1)
        elif strategy == "threshold":
            results = results.applymap(lambda x: x >= 0.5)
        elif strategy == "regression":
            pass
        else:
            raise ValueError("Invalid predict strategy")

        return results
